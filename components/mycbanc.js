var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var newsfeed_content = require('../components/newsfeed_content.js');
var doc_list = require('ui_documents/components/list.js');
var qna_list = require('ui_qna/components/list.js');
var ui_utils = require('ui_utils');
var ui_assets = require('ui_assets');

var newsfeed_tab = function (state) {
	state = state || {};
	var view_model = state.view_model || {};
	var my_cbanc = view_model.my_cbanc || {};
	var totals = (my_cbanc.totals || [])[0] || null;
	var newsdata = my_cbanc.newsfeed || [];
	var reputation = view_model.reputation_scores || {};
	var assets = ui_assets(state);

	return h('section', [
		h('main.feedlist', [
			h('h3', 'Your News Feed'),
			h('p', 'Your daily dose of industry news'),
			news_list(newsdata || [])
		]),
		h('aside.engagement', [
			h('div.my_reputation', [
				h('p.title', 'My Reputation'),
				h('p', [
					'Reputation Score: ' + (reputation.reputation_score || reputation.reputation_score === 0 ? reputation.reputation_score.toString() : 'N/A')
				]),
				h('p', [
					h('i.fa.fa-pie-chart'),
					'In the Top ' + (reputation.reputation_rank ? reputation.reputation_rank + '%' : 'N/A') + ' of CBANC Members'
				]),
				h('p', [
					h('i.fa.fa-pie-chart'),
					'Moved ' + (reputation.reputation_trend ? reputation.reputation_trend : 'N/A') + ' spots this week'
				]),
				h('p', [
					anchor({
						href: '/reputation/leaderboard',
						text: 'See Leaderboard >>'
					})
				])
			]),
			h('div.docs', [
				anchor({
					text: 'Download & Share Policy Documents',
					href: '/documents'
				}, 'main_cta'),
				anchor({
					text: 'You\'ve downloaded ' + (totals.downloaded_docs || totals.downloaded_docs === 0 ? totals.downloaded_docs.toString() : 'N/A') + ' Docs',
					href: '/documents/my-documents'
				}, 'content_count'),
				anchor({
					text: 'You\'ve uploaded ' + (totals.uploaded_docs ? totals.uploaded_docs : 'N/A') + ' Docs',
					href: '/documents/my-documents'
				}, 'content_count'),
				anchor({
					text: 'Upload a Document >>',
					href: '/documents/create'
				}, 'learn_more')
			]),
			h('div.qna', [
				anchor({
					text: 'Ask & Answer Questions',
					href: '/questions'
				}, 'main_cta'),
				anchor({
					text: 'You\'ve asked ' + (totals.asked_questions ? totals.asked_questions : 'N/A') + ' Questions',
					href: '/questions/my-qna'
				}, 'content_count'),
				anchor({
					text: 'You\'ve answered ' + (totals.answered_questions ? totals.answered_questions : 'N/A') + ' Questions',
					href: '/questions/my-qna'
				}, 'content_count'),
				anchor({
					text: 'Ask a Question >>',
					href: '/questions/create'
				}, 'learn_more')
			])
		])
	])
};

var docs_tab = function (view_model) {
	var my_cbanc = view_model.my_cbanc || {};
	var latest_docs = my_cbanc.latest_documents || [];
	var doc_requests = my_cbanc.doc_requests || [];

	return h('section', [
		h('main', [
			h('h3', 'Recently Uploaded Docs'),
			doc_list(latest_docs || [], 'card', {
				synopsis: true,
				signature: true,
				time: true,
				tags_count: 1
			})
		]),
		h('aside', [
			h('h3', 'Document Requests'),
			qna_list(doc_requests || [], 'card', {
				tags_count: 1
			})
		])
	])
};

var qna_tab = function (view_model) {
	var my_cbanc = view_model.my_cbanc || {};
	var latest_qna = my_cbanc.latest_questions || [];
	var latest_answers = my_cbanc.latest_answers || [];

	return h('section', [
		h('main', [
			h('h3', 'Recently Asked Questions'),
			qna_list(latest_qna || [], 'card')
		]),
		h('aside', [
			h('h3', 'Recently Answered'),
			qna_list(latest_answers || [], 'card')
		])
	])
};

var build_rep_tab = function (view_model) {
	var my_cbanc = view_model.my_cbanc || {};
	var reputation = view_model.reputation_scores || {};
	var doc_requests = my_cbanc.doc_requests || [];
	var unanswered_questions = my_cbanc.unanswered_questions || [];

	return h('section', [
		h('h2', 'Build Your Reputation'),
		h('p.current_reputation', 'Your current Reputation Score is ' + (reputation.reputation_score || reputation.reputation_score === 0 ? reputation.reputation_score.toString() : 'N/A')),
		h('p', 'The best way to build your Reputation is by providing Documents and Answers that are useful to other Members.'),
		anchor({
			text: 'Upload a Document',
			href: '/documents/create'
		}, 'upload_doc'),
		anchor({
			text: 'Answer a Question',
			href: '/questions'
		}, 'answer_question'),
		h('main', [
			h('h3', 'Unanswered Questions'),
			qna_list(unanswered_questions || [], 'card')
		]),
		h('aside', [
			h('h3', 'Document Requests'),
			qna_list(doc_requests || [], 'card', {
				tags_count: 1
			})
		])
	])
};

var news_list = function (newsdata) {
	var newsdata = newsdata || {};

	if (!newsdata) {
		return null
	}
	return newsdata.map(newsfeed_content)
};

module.exports = versionify(function (state) {
	var view_model = state.view_model || {};
	var page_model = state.page_model || {};
	var user = state.user || {};
	var my_cbanc = view_model.my_cbanc || {};
	var totals = (my_cbanc.totals || [])[0] || null;
	var newsdata = my_cbanc.newsfeed || [];
	var latest_docs = my_cbanc.latest_documents || [];
	var doc_requests = my_cbanc.doc_requests || [];
	var latest_qna = my_cbanc.latest_questions || [];
	var latest_answers = my_cbanc.latest_answers || [];
	var unanswered_questions = my_cbanc.unanswered_questions || [];
	var newsfeed_tab_handler = ui_utils.state.get().channels()['mycbanc.newsfeed.tab.click'];
	var docs_tab_handler = ui_utils.state.get().channels()['mycbanc.docs.tab.click'];
	var qna_tab_handler = ui_utils.state.get().channels()['mycbanc.qna.tab.click'];
	var build_rep_tab_handler = ui_utils.state.get().channels()['mycbanc.build_rep.tab.click'];

	return h('article#mycbanc', [
		h('section#hero', [
			h('h2', 'Welcome to CBANC, ' + user.person.first_name)
		]),
		h('hr'),
		h('section#dashboard', [
			h('section#controls', [
				h('ul.feed', [
					h('li.title', ['CBANC Feed']),
					h('li', [
						h('a', {
							'ev-click': newsfeed_tab_handler,
							id: (my_cbanc.a_newsfeed ? my_cbanc.a_newsfeed.id : null)
						}, 'All Activity')
					]),
					h('li', [
						h('a', {
							'ev-click': docs_tab_handler,
							id: (my_cbanc.a_docs ? my_cbanc.a_docs.id : null)
						}, 'Documents')
					]),
					h('li', [
						h('a', {
							'ev-click': qna_tab_handler,
							id: (my_cbanc.a_qna ? my_cbanc.a_qna.id : null)
						}, 'Q&A')
					]),
					h('li', [
						h('a', {
							'ev-click': build_rep_tab_handler,
							id: (my_cbanc.a_build_rep ? my_cbanc.a_build_rep.id : null)
						}, 'Build Your Rep')
					])
				])
			]),
			h('section#content', [
				h('section#documents_activity.content_pane', [
					(my_cbanc.show_docs_tab ? docs_tab(view_model) : null)
				]),
				h('section#questions_activity.content_pane', [
					(my_cbanc.show_qna_tab ? qna_tab(view_model) : null)
				]),
				h('section#build_your_reputation.content_pane', [
					(my_cbanc.show_build_rep_tab ? build_rep_tab(view_model) : null)
				]),
				h('section#newsfeed.content_pane', [
					(my_cbanc.show_newsfeed_tab ? newsfeed_tab(state) : null)
				])
			])
		])
	])
});