var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var signature = require('ui_signature/components/public.js');
var section = require('ui_cbw_static/components/content_section.js');

var testimonials = function (testimonials) {

  if (testimonials.length > 0) {

    return h('section#testimonials',
      h('div.quotes', [
        h('article', [
          h('div.sig', [
            signature(testimonials[0], testimonials[0].org, testimonials[0].created_date, 'Quoted') // only one for now
          ]),
          h('div.quote', [
            '"Over the years I have become a frequent user of CBANC as a primary source to financial industry data. The materials that are provided and shared through CBANC are priceless, because they cover so many areas of operations, risk and compliance. By having such a great shared network, it has saved us valuable time in researching many topics. I am grateful for having this resource center, that has also provides us with a networking with others in the industry."'
          ])
        ])
      ])
    );
  }

  return null;
};

module.exports = versionify(function (state) {
  state = state || {};
  var view_model = state.view_model || {};
  var page_model = state.page_model || {};
  var join = view_model.join_action || null;

  return h('div#home_public', [

    h('section#hero', [
      h('div.message', [
        h('h1', 'Over 7,000 banks and credit unions working together to keep community financial institutions independent and thriving.'),
        h('h3', [
          'CBANC is the #1 professional network for',
          h('br'),
          'bank and credit union professionals in the U.S.'
        ]),
        anchor({
          href: join,
          text: 'Join for Free'
        }, 'button')
      ])
    ]),

    h('h3.headline',
      h('span','What can you do on CBANC?')
    ),

    h('section#cta_menu', [
      h('div.wrapper', [

        h('div.documents', [
          h('h3', anchor({
            text: [
              'Download and Share',
              h('br'),
              'Policy Documents',
            ],
            href: '#documents'
          }, 'cta_link')),
          anchor({
            text: 'Learn more',
            href: '#documents'
          }, 'learn_more')
        ]),

        h('div.qna', [
          h('h3', anchor({
            text: [
              'Ask & Answer',
              h('br'),
              'Questions',
            ],
            href: '#questions'
          }, 'cta_link')),
          anchor({
            text: 'Learn more',
            href: '#questions'
          }, 'learn_more')
        ]),

        h('div.vendors', [
          h('h3', anchor({
            text: [
              'Research & Connect',
              h('br'),
              'with Vendors',
            ],
            href: '#vendors'
          }, 'cta_link')),
          anchor({
            text: 'Learn more',
            href: '#vendors'
          }, 'learn_more')
        ]),

        h('div.vendor_management', [
          h('h3', anchor({
            text: [
              'Risk Assess & Manage',
              h('br'),
              'Vendors',
            ],
            href: '#vendor_management'
          }, 'cta_link')),
          anchor({
            text: 'Learn more',
            href: '#vendor_management'
          }, 'learn_more')
        ]),

        h('div.education', [
          h('h3', anchor({
            text: [
              'Stay Sharp with',
              h('br'),
              'Continuing Education',
            ],
            href: '#education'
          }, 'cta_link')),
          anchor({
            text: 'Learn more',
            href: '#education'
          }, 'learn_more')
        ]),

        h('div.reputation', [
          h('h3', anchor({
            text: [
              'Build Your Industry',
              h('br'),
              'Reputation & Career',
            ],
            href: '#reputation'
          }, 'cta_link')),
          anchor({
            text: 'Learn more',
            href: '#reputation'
          }, 'learn_more')
        ])
      ])
    ]),

    testimonials(view_model.testimonials || []),

    h('section#sections', [

      section({
        html_class: 'documents',
        dark: true,
        href: join,
        side: 'left',
        title: [
          'Download and Share',
          h('br'),
          'Policy Documents'
        ],
        content: h("ul", [
          h("li",
            "Find real documents, used by your peers"
          ),
          h("li",
            "Share your work on policies, procedures, and training docs"
          ),
          h("li",
            "Learn the Pros and Cons of a Product before you purchase"
          )
        ])
      }),

      section({
        html_class: 'questions',
        dark: false,
        href: join,
        side: 'right',
        title: [
          'Ask & Answer',
          h('br'),
          'Questions'
        ],
        content: h("ul", [
          h("li",
            "Reach out to a nationwide network community bankers and credit union professionals"
          ),
          h("li",
            "Find real answers to your banking and credit union-related questions"
          ),
          h("li",
            "Help your peers and share your knowledge and experience"
          )
        ])
      }),

      section({
        html_class: 'vendors',
        dark: true,
        href: join,
        side: 'left',
        title: [
          'Research & Connect',
          h('br'),
          'with Vendors'
        ],
        content: h("ul", [
          h("li",
            "Browse thousands of real member reviews"
          ),
          h("li",
            "See how your peers think about other vendors"
          ),
          h("li",
            "Include the best vendors for your institution in your next RFP"
          ),
          h("li",
            "Share your experiences with your peers"
          )
        ])
      }),

      section({
        html_class: 'vendor_management',
        dark: false,
        button_text: 'Learn More',
        href: '/vendor-management',
        side: 'right',
        title: [
          'Risk Assess & Manage',
          h('br'),
          'Vendors'
        ],
        content: h("ul", [
          h("li",
            "Manage your vendors with ease"
          ),
          h("li",
            "Secure, community-powered compliance"
          ),
          h("li",
            "Stay connected. Informed. Compliant."
          )
        ])
      }),

      section({
        html_class: 'education',
        dark: true,
        button_text: 'Learn More',
        href: '/education',
        side: 'left',
        title: [
          'Stay Sharp with Continuing',
          h('br'),
          'Education'
        ],
        content: h("ul", [
          h("li",
            "Practical training from expert speakers on compliance, payments, lending, security, social media, and other banking challenges"
          ),
          h("li",
            "Stay current with the latest regulations and rules, examiner emphasis points, and industry trends"
          ),
          h("li",
            "Join our All-Access program for unlimited institution-wide access to more than 100 past and future training sessions"
          )
        ])
      }),

      section({
        html_class: 'reputation',
        dark: false,
        button_text: 'Learn More',
        href: join,
        side: 'right',
        title: [
          'Build Your Industry',
          h('br'),
          'Reputation & Career'
        ],
        content: h("ul", [
          h("li",
            "Build your reputation as you help your peers"
          ),
          h("li",
            "Establish yourself as a thought leader by demonstrating your knowledge"
          ),
          h("li",
            "Provide feedback, helping other members find the best content"
          )
        ])
      })
    ])
  ])
});