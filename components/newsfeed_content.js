var h = require('mercury').h;
var versionify = require('../versionify.js');
var anchor = require('ui_link');
var time = require('ui_helpers/components/time.js');

var urlHost = function (str) {
  pathArray = str.split('/');
  host = pathArray[2];
  return host.replace(/^[^.]+\./g, "");
}

var ditchHTML = function (str) {
  var clean = str.replace(/<(?:.|\n)*?>/gm, '');
  return clean;
}

module.exports = versionify(function (newsfeed) {
  return h("article", {
    "data-article-id": newsfeed.id
  }, [
    h("header", [
      anchor({
        href: newsfeed.link,
        target: '_blank',
        text: newsfeed.title
      }, 'headline'),
      h("br"),
      h("spam.meta_info", [
        h("time", {
          "datetime": time(newsfeed.pub_date)
        }, [time(newsfeed.pub_date)]),
        " - ",
        anchor({
          text: urlHost(newsfeed.link),
          href: newsfeed.link,
          target: '_blank'
        }, 'article_host')
      ])
    ]),

    h("p", [ditchHTML(newsfeed.summary), h("a.read_full_story", {
      "href": newsfeed.link,
      "target": "_blank"
    }, ["Read full story..."])])
  ])
});