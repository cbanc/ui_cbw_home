var h = require('mercury').h;
var versionify = require('../versionify.js');
var public_home = require('../components/public.js');
var mycbanc = require('../components/mycbanc.js');

module.exports = versionify(function (state) {
  state = state || {};
  var user = state.user || {};

  if (user.logged_in) {
    return mycbanc(state);
  } else {
    return public_home(state)
  }
});
