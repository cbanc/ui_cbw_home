#ui_cbw_home

CBANC homepage repository. Each user scenario is split into components and the main controller decides which one to deliver depending on what that user has permission to. Currently, it only supports a logged_in boolean.

I could see this repo being updated regularly and expanded as our engagement gets more sophisticated.

##Basic scenario fork
This is from /main/home.js:
```
#!js

if (user.logged_in) {
  return mycbanc(state);
} else {
  return public_home(state)
}

```