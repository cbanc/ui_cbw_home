var gulp = require('gulp');

var options = {
  name: 'home',
  page_module: true
};

var ui_gulp = require('ui_gulp')(options);

gulp.task('default', ui_gulp.default);

gulp.task('watch', ui_gulp.watch);