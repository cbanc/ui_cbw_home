var httpContext = {
  controller: {
    name: 'home_view'
  }
};

module.exports = {
  user: {
    person: {
      full_name: 'Bill Murray',
      reputation: {
        thanks_count: 35
      }
    },
    logged_in: true
  },
  view_model: {
    join_action: {
      text: 'Join',
      href: '/join'
    },
    reputation_scores: {
      reputation_trend: -13,
      reputation_rank: 87,
      reputation_score: 0
    },
    my_cbanc: {
      show_newsfeed_tab: false,
      show_docs_tab: true,
      show_qna_tab: false,
      show_build_rep_tab: false,
      a_newsfeed: {
        href: 'home',
        action: 'newsfeed',
        text: 'All Activity',
        id: 'show_newsfeed',
      },

      a_docs: {
        href: 'home',
        action: 'documents',
        text: 'Documents',
        id: 'show_documents'
      },

      a_qna: {
        href: 'home',
        action: 'qna',
        text: 'All Activity',
        id: 'show_qna'
      },

      a_build_rep: {
        href: 'home',
        action: 'build_your_rep',
        text: 'Build Your Rep',
        id: 'show_newsfeed'
      },
      unanswered_questions: [{
        title: "Sports Trivia",
        question: "Which competitive modern sport traces its roots to the English game shinny, the Irish game hurley, and the Scottish game shinty? ",
        active: true,
        id: 2696,
        created_by: {
          id: 2612,
          key: "d5c35c72-445a-4c77-9cb4-de2ea7ec62f8",
          title: "Jake the Dog",
          first_name: "Jake",
          last_name: "",
          functional_area: "Criminal",
          functional_level: "CFO",
          href: "/users/view/2612/"
        },
        created_date: "2015-10-13T17:18:36.117Z",
        reputation: -1,
        org: {
          key: "1021e256-fbf5-4032-9926-0764836fe2f2",
          id: 2658,
          href: "Route%20Not%20Found%20-%20orgs/view"
        },
        tags: [{
          name: "Science",
          key: "ae2eb33a-73c6-45c4-81cd-42ba2b6787b1",
          alias: ["Art"]
        }],
        url: "/questions/view/5e9ac118-71c5-11e5-9be5-7831c1baaeec/"
      }],
      latest_answers: [],
      latest_questions: [{
        question: {
          document_request: true,
          anonymous: false,
          text: "I am here. Are you there? Let's chat. ",
          title: "Hello? Is anyone there? Can I have a Document?",
          active: true,
          key: "a7e4d610-2b08-40c9-9f0a-6130d3cf0387",
          id: 2400
        },
        tags: [],
        answers: 0
      }, {
        question: {
          document_request: false,
          anonymous: false,
          text: "Do you do bank things? Like money? Can handle a vault? Thanks!",
          title: "Are you a banker?",
          active: true,
          key: "1ea2eaf7-3568-43b3-b0e9-fed378df98bc",
          id: 2403
        },
        tags: [],
        answers: 0
      }],
      latest_documents: [{
        tags: [],
        thanks: 0,
        examtested: false,
        synopsis: "This is the badge for my soccer club, Austin Athletic Club.",
        file_extension: "gif",
        title: "AAC Badge",
        active: true,
        key: "11c5384b-f0b3-4fc0-9a3c-031cabe02510",
        id: 2397,
        downloads: 0,
        comments: 0
      }, {
        tags: [],
        thanks: 0,
        examtested: false,
        synopsis: "A 3-page guide including everything you need to single handedly run Springfield Power Plant.",
        file_extension: "Welcome to the Club",
        title: "Nuclear Power Plant Handbook",
        active: true,
        key: "46a9027d-992a-4187-bad7-25fba6078892",
        id: 2346,
        downloads: 0,
        comments: 6
      }, {
        tags: [],
        thanks: 0,
        examtested: false,
        synopsis: "Proposed changes for US Soccer's crest design.",
        file_extension: "png",
        title: "US Soccer Crest",
        active: true,
        key: "8f1eac7c-1a55-40dc-a798-4ce1c0d651d3",
        id: 2399,
        downloads: 0,
        comments: 0
      }, {
        tags: [],
        thanks: 0,
        examtested: false,
        synopsis: "Duke it out!",
        file_extension: "jpg",
        title: "Jason v Tommy",
        active: true,
        key: "41860047-eba6-4baa-9ef2-02f4768cac93",
        id: 2402,
        downloads: 0,
        comments: 0
      }],
      totals: [{
        reputation: 0,
        answered_questions: 0,
        uploaded_docs: 4,
        downloaded_docs: 0,
        asked_questions: 2
      }],
      newsfeed: [{
        summary: "<a href='http: //www.nytimes.com/2015/08/23/magazine/why-new-orleans-black-residents-are-still-under-water-after-katrina.html?partner=rssnyt&emc=rss'><img src='http://static01.nyt.com/images/2015/08/23/magazine/23katrina1/23mag-23katrina-t_CA3-thumbStandard.jpg border='0' height='75' width='75' hspace='4' align='left'/></a>One black-owned bank helped build the city\'s African-American middle class â€” until the hurricane destroyed much more than their homes.",
        import_date: "2015-08-19T12:01:23.756Z",
        categories: [],
        link: "http://www.nytimes.com/2015/08/23/magazine/why-new-orleans-black-residents-are-still-under-water-after-katrina.html?partner=rssnyt&emc=rss",
        pub_date: "2015-08-23T04:00:00.000Z",
        origin_feed: "topics.nytimes.com/top/reference/timestopics/subjects/b/banks_and_banking/index.html?rss=1",
        image: "",
        author: "By GARY RIVLIN",
        description: "<a href='http: //www.nytimes.com/2015/08/23/magazine/why-new-orleans-black-residents-are-still-under-water-after-katrina.html?partner=rssnyt&emc=rss'><img src='http://static01.nyt.com/images/2015/08/23/magazine/23katrina1/23mag-23katrina-t_CA3-thumbStandard.jpg' border='0' height='75' width='75' hspace='4' align='left'/></a>One black-owned bank helped build the city\'s African-American middle class â€” until the hurricane destroyed much more than their homes.",
        title: "Why New Orleans\'s Black Residents Are Still Underwater After Katrina",
        key: "f8df480c-bd76-47f8-9c81-c2823746ae7f",
        id: 33786
      }, {
        summary: "By&#160;Bernard Lunn The old wisdom was that your biggest financial decision was which house to buy and how to finance it. That has been replaced by the higher education decision &#8211; whether to invest in getting a college education and if so which college and then how to finance it. In the old days, the&#8230;<a href='http: //dailyfintech.com/2015/08/21/education-fintech-startups-cannot-change-the-math-of-education-inflation/''>Read more <span>Education Fintech startups cannot change the math of education&#160;inflation</span></a><img alt='' border='0' src='http://pixel.wp.com/b.gif?host=dailyfintech.com&#38;blog=70326731&#38;post=1778&#38;subd=fintech4us&#38;ref=&#38;feed=1' width='1' height='1'>",
        import_date: "2015-08-21T12:01:38.802Z",
        categories: [
          "Newswire",
          "consumer banking"
        ],
        link: "http://bankinnovation.net/2015/08/education-fintech-startups-cannot-change-the-math-of-education-inflation/",
        pub_date: "2015-08-21T04:32:50.000Z",
        origin_feed: "http://bankinnovation.net/feed/",
        image: "",
        author: "Bernard Lunn",
        description: "By&#160;Bernard Lunn The old wisdom was that your biggest financial decision was which house to buy and how to finance it. That has been replaced by the higher education decision &#8211; whether to invest in getting a college education and if so which college and then how to finance it. In the old days, the&#8230;<a href='http: //dailyfintech.com/2015/08/21/education-fintech-startups-cannot-change-the-math-of-education-inflation/'>Read more <span>Education Fintech startups cannot change the math of education&#160;inflation</span></a><img alt='' border='0' src='http://pixel.wp.com/b.gif?host=dailyfintech.com&#38;blog=70326731&#38;post=1778&#38;subd=fintech4us&#38;ref=&#38;feed=1' width='1' height='1'>",
        title: "Education Fintech startups cannot change the math of education inflation",
        key: "b4f03bb6-2a23-45aa-8294-086ca8e83fca",
        id: 34664
      }, {
        summary: "The money from other eurozone countries is part of a much bigger bailout package that cleared the last remaining political hurdles this week.",
        import_date: "2015-08-20T12:01:40.952Z",
        categories: [],
        link: "http://www.nytimes.com/2015/08/21/business/international/greece-bailout-debt.html?partner=rssnyt&emc=rss",
        pub_date: "2015-08-21T04:00:00.000Z",
        origin_feed: "topics.nytimes.com/top/reference/timestopics/subjects/b/banks_and_banking/index.html?rss=1",
        image: "",
        author: "By JACK EWING",
        description: "The money from other eurozone countries is part of a much bigger bailout package that cleared the last remaining political hurdles this week.",
        title: "Greece Gets Nearly $14.4 Billion to Pay E.C.B. and I.M.F.",
        key: "99859ea2-7c53-4344-b949-e7d579471f26",
        id: 34295
      }, {
        summary: "Money from other eurozone countries arrived just in time for the country to repay â‚¬3.2 billion that was due on government bonds held by the central bank.",
        import_date: "2015-08-21T12:01:26.373Z",
        categories: [],
        link: "http://www.nytimes.com/2015/08/21/business/international/greece-bailout-debt.html?partner=rssnyt&emc=rss",
        pub_date: "2015-08-21T04:00:00.000Z",
        origin_feed: "topics.nytimes.com/top/reference/timestopics/subjects/b/banks_and_banking/index.html?rss=1",
        image: "",
        author: "By JACK EWING",
        description: "Money from other eurozone countries arrived just in time for the country to repay â‚¬3.2 billion that was due on government bonds held by the central bank.",
        title: "Greece Makes Payment to European Central Bank, Avoiding Default",
        key: "806b6b0f-ba62-4305-ae5a-08994f63e2f2",
        id: 34533
      }, {
        summary: "<a href='http: //www.nytimes.com/2015/08/21/opinion/paul-krugman-debt-is-good-for-the-economy.html?partner=rssnyt&emc=rss'><img src='http://static01.nyt.com/images/2014/11/12/opinion/krugman-circular/krugman-circular-thumbStandard-v2.png' border='0' height='75' width='75' hspace='4' align='left'/></a>A problem with the economy may be that we aren\''t in deep enough, not that we\'re in too deep.",
        import_date: "2015-08-21T12:01:26.137Z",
        categories: [],
        link: "http://www.nytimes.com/2015/08/21/opinion/paul-krugman-debt-is-good-for-the-economy.html?partner=rssnyt&emc=rss",
        pub_date: "2015-08-21T04:00:00.000Z",
        origin_feed: "topics.nytimes.com/top/reference/timestopics/subjects/b/banks_and_banking/index.html?rss=1",
        image: "",
        author: "By PAUL KRUGMAN",
        description: "<a href='http: //www.nytimes.com/2015/08/21/opinion/paul-krugman-debt-is-good-for-the-economy.html?partner=rssnyt&emc=rss'><img src='http://static01.nyt.com/images/2014/11/12/opinion/krugman-circular/krugman-circular-thumbStandard-v2.png' border='0' height='75' width='75' hspace='4' align='left'/></a>A problem with the economy may be that we aren\''t in deep enough, not that we\'re in too deep.",
        title: "Debt Is Good",
        key: "0d74ea77-5b54-41bc-8a0a-8b36703cb5a5",
        id: 34531
      }, {
        summary: "We drive on parkways and park on driveways. Cigarettes are sold in gas stations even though smoking is prohibited there. Fat chance and slim chance mean the same thing. Phonetic isn\'t spelled the way it sounds. When it comes to retirement plans,...<BR />By: <a href='http: //www.jdsupra.com/profile/Ary_Rosenbaum/'>Ary Rosenbaum</a>",
        import_date: "2015-08-21T12:01:27.104Z",
        categories: [],
        link: "http://www.jdsupra.com/legalnews/absurd-things-about-401k-plans-that-ar-00722/",
        pub_date: "2015-08-21T02:58:41.000Z",
        origin_feed: "http://www.jdsupra.com/resources/syndication/docsRSSfeed.aspx?ftype=FinanceBanking",
        image: "",
        author: "Ary Rosenbaum",
        description: "We drive on parkways and park on driveways. Cigarettes are sold in gas stations even though smoking is prohibited there. Fat chance and slim chance mean the same thing. Phonetic isn\'t spelled the way it sounds. When it comes to retirement plans,...<BR />By: <a href='http: //www.jdsupra.com/profile/Ary_Rosenbaum/'>Ary Rosenbaum</a>",
        title: "Absurd Things About 401(k) Plans That Are True",
        key: "2e6f25a6-e5e4-4a8e-a4f0-c1f662742df4",
        id: 34545
      }, {
        summary: "<p>Fintech funding records continue to fall. Last week was the record number of deals, with 27. This week it was the biggest total dollars raised at $1.63 billion, from 25 companies. The vast majority of the money ($1.54 billion) went to three online lenders (Sofi = $1 billion, Avant = $340 million, Dianrong = $220 <a class='moretag' href='http: //finovate.com/fintech-fundings-25-companies-raise-1-6-billion-week-ending-aug-20/'>Read more...</a></p> <p>The post <a rel='nofollow' href='http://finovate.com/fintech-fundings-25-companies-raise-1-6-billion-week-ending-aug-20/'>Fintech Fundings: 25 Companies Raise $1.6 Billion Week Ending Aug 20</a> appeared first on <a rel='nofollow' href='http://finovate.com'>Finovate</a>.</p>",
        import_date: "2015-08-21T12:01:39.417Z",
        categories: [
          "Uncategorized"
        ],
        link: "http://feedproxy.google.com/~r/netbanker/~3/XAKkVTmUKwo/",
        pub_date: "2015-08-21T02:46:50.000Z",
        origin_feed: "http://feeds.feedburner.com/netbanker",
        image: "",
        author: "Jim Bruene",
        description: "<p><a href='http: //finovate.com/wp-content/uploads/2015/04/ManwithMoney_clipart.jpg'><img class='alignright wp-image-45174 img-responsive' src='http://finovate.com/wp-content/uploads/2015/04/ManwithMoney_clipart.jpg' alt='Man_money' width='184' height='221' /></a>Fintech funding records continue to fall. Last week was the record number of deals, with 27. This week it was the biggest total dollars raised at $1.63 billion, from 25 companies. The vast majority of the money ($1.54 billion) went to three online lenders (Sofi = $1 billion, Avant = $340 million, Dianrong = $220 million). As private companies, there is not a lot of transparency about the deal terms, but we expect most, if not all, of the funding was structured as debt, not equity.</p> <p>One Finovate alum was in the mix this week:Â <strong>Tio Networks</strong> picked up $1.7 million in a secondary stock offering. So far this year, 94 Finovate alums have raised $2.9 billion.</p> <p>Here are the deals by size from August 14Â to Aug 20:</p> <p><strong><a href='https://www.sofi.com/''>SoFI</a> (Social Finance)</strong><br /> <i>Student loan marketplace lender</i><br /> HQ: San Francisco, California<br /> Latest round: $1 billion ($4 billion valuation)<br /> Total raised: $1.75 billion (at least $400 million is debt)<br /> Tags: Student loans, credit, underwriting, refinance, crowdfunding, P2P, investing<br /> Source:Â <a href='http://www.wsj.com/articles/online-lender-social-finances-latest-fundraising-implies-4-billion-valuation-1440009125'>Wall Street Journal</a></p> <p><a href='https://www.avant.com/'><strong>Avant</strong></a><br /> <em>Alt-consumer lending</em><br /> HQ: Chicago, Illinois<br /> Latest round: $339 million (Debt)<br /> Total raised: $1.43 billion ($1.1 billion debt, $330 million equity)<br /> Tags: Consumer credit, underwriting<br /> Source: <a href='http://fortune.com/2015/08/20/avant-banks-lender-financing/''>Fortune</a></p> <p><a href='http://www.dianrong.com/''><strong>Dianrong</strong></a><br /> <i>Marketplace lender and digital banking solutions provider</i><br /> HQ: Shanghai,Â China<br /> Latest round: $207 million Series C (<a href='http://www.bloomberg.com/news/articles/2015-06-17/china-online-lender-said-raising-funds-at-about-1-billion-value'>$1 billion valuation</a>)<br /> Total raised:Â $219 million<br /> Tags:Â Lending, digital banking, P2P, credit, consumer, SMB, underwriting, Standard Charter (lead investor)<br /> Source:Â <a href='http://db.ftpartners.com/TransactionDetail?QS=Standard-Chartered-PE-Arm-Invests-in-China-s-Online-Lender-Dianrong-com'>FT Partners</a></p> <p><a href='http://www.launchpointcorporation.com/'><strong>LaunchPoint</strong></a><br /> <em>Health care payments &amp; analytics software</em><br /> HQ: Goleta, California<br /> Latest round: $22.5 million<br /> Total raised: $26 million<br /> Tags: Payments, insurance, BI, analytics, enterprise<br /> Source: <a href='http://db.ftpartners.com/TransactionDetail?QS=LaunchPoint-Secures-22-5-mm-in-Financing-from-Carrick-Capital-Partners'>FT Partners</a></p> <p><a href='https://revelsystems.com/'><strong>RevelÂ Systems</strong></a><br /> <i>iPad-based point-of-sale system</i><br /> HQ: San Francisco, California<br /> Latest round: $13.5 million ($500+ million valuation)<br /> Total raised: $129 million<br /> Tags: Merchants, SMB, card acquiring, POS, mobile, tablet<br /> Source: <a href='https://www.crunchbase.com/organization/revel-systems'>Crunchbase</a></p> <p><a href='https://www.grouplend.ca/''><strong>GroupLend</strong></a>",
        title: "Fintech Fundings: 25 Companies Raise $1.6 Billion Week Ending Aug 20",
        key: "780d7e7d-f160-4365-b25a-70c7716f1a6e",
        id: 34666
      }, {
        summary: "Overview of the Pay Ratio Rule: The Basic Rule - Reporting companies subject to the rule (Subject Companies) must disclose the ratio (pay ratio) for the most recent fiscal year of (a) the median of total compensation of all employees of the...<BR />By: <a href='http: //www.jdsupra.com/profile/morgan-lewis/''>Morgan Lewis</a>",
        import_date: "2015-08-21T12:01:26.503Z",
        categories: [],
        link: "http://www.jdsupra.com/legalnews/sec-adopts-pay-ratio-disclosure-rule-67503/",
        pub_date: "2015-08-21T01:38:32.000Z",
        origin_feed: "http://www.jdsupra.com/resources/syndication/docsRSSfeed.aspx?ftype=FinanceBanking",
        image: "",
        author: "Morgan Lewis",
        description: "Overview of the Pay Ratio Rule: The Basic Rule - Reporting companies subject to the rule (Subject Companies) must disclose the ratio (pay ratio) for the most recent fiscal year of (a) the median of total compensation of all employees of the...<BR />By: <a href='http: //www.jdsupra.com/profile/morgan-lewis/''>Morgan Lewis</a>",
        title: "SEC Adopts Pay Ratio Disclosure Rule",
        key: "16e6aadb-c8bf-40ef-92aa-ae57519c9f82",
        id: 34535
      }, {
        summary: "The US Securities and Exchange Commission (SEC) is investigating two prominent US banks for ties to Mexican drug cartels. Reuters has reported that the SEC is conducting a sweep of the brokerage industry to ensure compliance with anti-money...<BR />By: <a href='http: //www.jdsupra.com/profile/WeComply/''>Thomson Reuters Compliance Learning</a>",
        import_date: "2015-08-21T12:01:27.542Z",
        categories: [],
        link: "http://www.jdsupra.com/legalnews/sec-probes-us-banks-for-mexican-drug-73839/",
        pub_date: "2015-08-21T01:35:22.000Z",
        origin_feed: "http://www.jdsupra.com/resources/syndication/docsRSSfeed.aspx?ftype=FinanceBanking",
        image: "",
        author: "Thomson Reuters Compliance Learning",
        description: "The US Securities and Exchange Commission (SEC) is investigating two prominent US banks for ties to Mexican drug cartels. Reuters has reported that the SEC is conducting a sweep of the brokerage industry to ensure compliance with anti-money...<BR />By: <a href='http: //www.jdsupra.com/profile/WeComply/''>Thomson Reuters Compliance Learning</a>",
        title: "SEC probes US Banks for Mexican Drug Cartel Ties",
        key: "dcf89ca2-b71b-4489-b1da-94f4fb3768b5",
        id: 34553
      }, {
        summary: "Ad Group to FTC: Keep Right to Be Forgotten Out of U.S. - Responding to a petition filed by Consumer Watchdog with the Federal Trade Commission seeking an expansion of the â€œRight to Be Forgottenâ€ to the United States, the Association of National...<BR />By: <a href='http: //www.jdsupra.com/profile/manatt/''>Manatt, Phelps & Phillips, LLP</a>",
        import_date: "2015-08-21T12:01:25.376Z",
        categories: [],
        link: "http://www.jdsupra.com/legalnews/advertising-law-august-2015-3-36467/",
        pub_date: "2015-08-21T01:15:00.000Z",
        origin_feed: "http://www.jdsupra.com/resources/syndication/docsRSSfeed.aspx?ftype=FinanceBanking",
        image: "",
        author: "Manatt, Phelps & Phillips, LLP",
        description: "Ad Group to FTC: Keep Right to Be Forgotten Out of U.S. - Responding to a petition filed by Consumer Watchdog with the Federal Trade Commission seeking an expansion of the â€œRight to Be Forgottenâ€ to the United States, the Association of National...<BR />By: <a href='http: //www.jdsupra.com/profile/manatt/''>Manatt, Phelps & Phillips, LLP</a>",
        title: "Advertising Law - August 2015 #3",
        key: "a4ba6e47-d400-43ef-bd00-509b7d6937de",
        id: 34516
      }],
      doc_requests: [{
        document_request: true,
        anonymous: false,
        text: 'I am here. Are you there? Let\'s chat. ',
        title: 'Hello? Is anyone there? Can I have a Document?',
        active: true,
        key: 'a7e4d610-2b08-40c9-9f0a-6130d3cf0387',
        id: 2400,
        answers: [],
        created_by: {
          id: 2292,
          key: '6815cfb1-ca95-4611-ad1b-9f2cb41843ab',
          title: 'Dad',
          first_name: 'Homer',
          last_name: 'Simpson',
          functional_area: 'Donuts',
          functional_level: 'CEO'
        },
        created_date: '2015-10-02T20:27:46.973Z',
        updated_by: null,
        org: {
          key: '5e95225e-bfa4-45d2-9956-0bb6b0f18174',
          id: 2318
        }
      }, {
        document_request: true,
        anonymous: false,
        text: 'I am here. Are you there? Let\'s chat. ',
        title: 'Hello? Is anyone there? Can I have a Document?',
        active: true,
        key: 'a7e4d610-2b08-40c9-9f0a-6130d3cf0387',
        id: 2400,
        answers: [],
        created_by: {
          id: 2292,
          key: '6815cfb1-ca95-4611-ad1b-9f2cb41843ab',
          title: 'Dad',
          first_name: 'Homer',
          last_name: 'Simpson',
          functional_area: 'Donuts',
          functional_level: 'CEO'
        },
        created_date: '2015-10-02T20:27:46.973Z',
        updated_by: null,
        org: {
          key: '5e95225e-bfa4-45d2-9956-0bb6b0f18174',
          id: 2318
        }
      }],
    },
    testimonials: [{
      id: 2282,
      updated_date: "2015-01-21T21:29:14.806Z",
      access_token: null,
      active: true,
      key: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
      created_date: "05/22/2015",
      email: "paula.bertels@callawaybank.com",
      email_preferences: {
        daily_digest: true,
        documents: true,
        qna: true,
        reviews: true,
        webinars: true
      },
      first_name: "Kevin",
      last_name: "Kappa",
      legacy_user_id: "7651",
      phone: [
        "5735926378"
      ],
      publish_email: false,
      publish_phone: false,
      title: "Chief Risk Officer",
      updated_by: "00000000-0000-0000-0000-000000000000",
      years_banking_experience: 0,
      _type: "person",
      full_name: "Kevin M. Kappa",
      password: null,
      org: {
        id: "6e811a1f66f3c9abfb94d6fd190cf2d0",
        updated_date: "2015-01-21T21:12:42.448Z",
        active: true,
        address: [
          "p.o. box 10"
        ],
        asset_size: 280,
        branches: 8,
        city: "Fulton",
        country: "US",
        created_by_user: "00000000-0000-0000-0000-000000000000",
        created_date: "2010-08-24T11:08:37.170Z",
        display_name: "The Callaway Bank",
        domain: [
          "callawaybank.com"
        ],
        fi_type: "bank",
        legal_name: "The Callaway Bank",
        phone: "5736423322",
        postal_code: "65251",
        regulatory_agencies: [
          "FRB"
        ],
        routing_number: "081501696",
        state: "MO",
        updated_by: "00000000-0000-0000-0000-000000000000",
        website: "http://callawaybank.com",
        _type: "fi_org"
      }
    }, {
      id: 2282,
      updated_date: "2015-01-21T21:29:14.806Z",
      access_token: null,
      active: true,
      key: "9d6b5e46-78a9-438a-abaa-1f8ce1e0d5a5",
      created_date: "05/22/2015",
      email: "paula.bertels@callawaybank.com",
      email_preferences: {
        daily_digest: true,
        documents: true,
        qna: true,
        reviews: true,
        webinars: true
      },
      first_name: "Steven",
      last_name: "Gerrard",
      legacy_user_id: "7651",
      phone: [
        "5735926378"
      ],
      publish_email: false,
      publish_phone: false,
      title: "Risk Management",
      updated_by: "00000000-0000-0000-0000-000000000000",
      years_banking_experience: 0,
      _type: "person",
      full_name: "Steven Gerrard",
      password: null,
      org: {
        id: "6e811a1f66f3c9abfb94d6fd190cf2d0",
        updated_date: "2015-01-21T21:12:42.448Z",
        active: true,
        address: [
          "p.o. box 10"
        ],
        asset_size: 440,
        branches: 8,
        city: "Fulton",
        country: "US",
        created_by_user: "00000000-0000-0000-0000-000000000000",
        created_date: "2010-08-24T11:08:37.170Z",
        display_name: "The Callaway Bank - Investments",
        domain: [
          "callawaybank.com"
        ],
        fi_type: "bank",
        legal_name: "The Callaway Bank",
        phone: "5736423322",
        postal_code: "65251",
        regulatory_agencies: [
          "FRB"
        ],
        routing_number: "081501696",
        state: "TX",
        updated_by: "00000000-0000-0000-0000-000000000000",
        website: "http://callawaybank.com",
        _type: "fi_org"
      }
    }]
  },
  httpContext: httpContext,
  config: require('../../tmp/js/code_gen/config.js'),
  page_model: {
    title: 'Welcome to CBANC Network',
    stylesheets: [
      './index.css',
    ],
    header: {
      mobile_menu_expanded: false,
      mobile_user_menu_expanded: false,
      menu_items: [{
        text: "Documents",
        href: "/documents"
      }, {
        text: "Q&A",
        href: "/questions"
      }, {
        text: "Products",
        href: "/products"
      }, {
        text: "Education",
        href: "/education"
      }, {
        text: "Vendor Management",
        href: "/vendormanagement"
      }],
      sign_in: {
        text: "Sign In",
        href: "https://login-edge.cbancnetwork.com/membership/login?client_id=123"
      },
      sign_out: {
        text: "Sign Out",
        href: "https://login-edge.cbancnetwork.com/membership/logout?client_id=123"
      },
      join: {
        text: "Join For Free",
        href: "https://login-edge.cbancnetwork.com/membership/join?client_id=123"
      }
    },
  }
};