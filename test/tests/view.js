var toHTML = require('vdom-to-html');
var assert = require('assert');

var render = require('../../components/view.js');
var data = require('../fixture/data.js');
var cheerio = require('cheerio');

suite('Component Render', function () {

  test('Render', function () {
    var dom;

    try {
      dom = render(data);
    } catch (e) {
      assert.ifError(e);
    }

    $ = cheerio.load(toHTML(dom));

    assert.ok($('#id a').length, 2, 'there are 2 links');

    console.log(toHTML(dom));
  });

  test('Render - Null data', function () {
    var dom;

    try {
      dom = render(null);
    } catch (e) {
      assert.ifError(e);
    }

    console.log(toHTML(dom));
  });

});
