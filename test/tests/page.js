var toHTML = require('vdom-to-html');
var assert = require('assert');
var _ = require('lodash');

var data = require('../fixture/dogs_page.js');

// ui_page needs a page table
var render = require('ui_page')(require('../fixture/page_table.js'));


suite('Page Render', function () {

  test('Render', function () {
    var dom;

    try {
      dom = render(data);
    } catch (e) {
      assert.ifError(e);
    }

    console.log(toHTML(dom));
  });

});
