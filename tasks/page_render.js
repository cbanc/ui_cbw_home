var assert = require('assert');

module.exports = function (data, page_table) {
  var dom;
  var render = require('ui_page')(page_table);

  try {
    dom = render(data);
  } catch (e) {
    assert.ifError(e);
  }
  return dom;
};
